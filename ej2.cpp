#include <iostream>
#include <fstream>
#include <pthread.h>
#include <string.h>
#include <time.h>

using namespace std;

//estructura ejercicio2
struct ejercicio2_estructura{
	ifstream archivo;
	int totalLineas;
	int totalPalabras;
	int totalCaracteres;
};

//funcion de la estrutura correspondiente a contar caracteres
void *contarCaracter(void *param){

	//variables generadas de la estructura
	ejercicio2_estructura *recibir;
	recibir = (ejercicio2_estructura *)param;

	//variables contadoras y de lineas
	int contadorCaracter = 0;
	char linea[3000];

	//ejecular solo en caso de archivo
	while(!recibir->archivo.eof()){
		recibir -> archivo.getline(linea, 3000);

		//contador de variables se les suma el largo de la linea(carcateres)
		contadorCaracter = contadorCaracter + strlen(linea);
	}

	//recibir el contador de caracteres en el total de la estructura
	recibir -> totalCaracteres = contadorCaracter;
	cout << "Cantidad de Caracteres: " << contadorCaracter << endl;
	pthread_exit(0);

}

//funcion de la estrutura correspondiente a contar palabras
void *contarPalabras(void *param){
	ejercicio2_estructura *recibir;
	recibir = (ejercicio2_estructura *)param;

	int contadorPalabras = 0;
	char linea[3000];

	//ejecular solo en caso de archivo
	while(!recibir->archivo.eof()){
		recibir -> archivo.getline(linea, 3000);

		//obtener largo de linea
		int larg = strlen(linea);
		for(int i=0;i<larg;i++){
			if((linea[i]==' ' || linea[i+1]== '\0')){ //condicion que limita los espacios como contar una palabra
			contadorPalabras++;
			}
		}
	}


	//recibir el contador de palabras en el total de la estructura
	recibir -> totalPalabras = contadorPalabras;
	cout << "Cantidad de Palabras: " << contadorPalabras << endl;
	pthread_exit(0);

}

//funcion de la estrutura correspondiente a contar lineas
void *contarLineas(void *param){
	ejercicio2_estructura *recibir;
	recibir = (ejercicio2_estructura *)param;
	int contadorLineas = 0;
	char linea[3000];

	//ejecular solo en caso de archivo
	while(!recibir->archivo.eof()){
		recibir -> archivo.getline(linea, 3000);

		//contar lineas
		contadorLineas++;
	}

	//recibir el contador de palabras en el total de la estructura
	recibir -> totalLineas = contadorLineas;
	cout << "Cantidad de lineas: " << contadorLineas << endl;
	pthread_exit(0);

}

float calcular_tiempo(float tiempoinicial, float tiempofinal){
	//formula para calcualr el tiempode ejecucion
	float ejecucion = (tiempofinal - tiempoinicial)/CLOCKS_PER_SEC;
	return ejecucion;
}

int main(int argc, char* argv[]){
	//en caso de no cargar archivo no ejecutar
	if(argc <= 1){
		cout << "sin archivos" << endl;
	}

	pthread_t threads[argc*3-1 ];
	ejercicio2_estructura contenido;

	//variables
	int contadorTotalLineas = 0;
	int contadorTotalPalabras = 0;
	int contadorTotalCaracter = 0;
	float tiempoinicial, tiempofinal;

	//inicio de calculo de tiempo inicial
	tiempoinicial = clock();

	//ciclo para leer todos los archivos cargados mediante la ejecucion
	for(int i=1;i<argc;i++){
		cout << endl;
		cout << "Nombre del archivo: " << argv[i] << endl;

		//abrir archivo, contar lineas y cerrar
		contenido.archivo.open(argv[i]);
		pthread_create(&threads[i-1], NULL, contarLineas, (void *)&contenido);
		pthread_join(threads[i-1], NULL);
		contenido.archivo.close();
		contadorTotalLineas = contadorTotalLineas + contenido.totalLineas;

		//abrir archivo, contar palabras y cerrar
		contenido.archivo.open(argv[i]);
		pthread_create(&threads[i-1], NULL, contarPalabras, (void *)&contenido);
		pthread_join(threads[i-1], NULL);
		contenido.archivo.close();
		contadorTotalPalabras = contadorTotalPalabras + contenido.totalPalabras;

		//abrir archivo, contar caracteres y cerrar
		contenido.archivo.open(argv[i]);
		pthread_create(&threads[i-1], NULL, contarCaracter, (void *)&contenido);
		pthread_join(threads[i-1], NULL);
		contenido.archivo.close();
		contadorTotalCaracter = contadorTotalCaracter + contenido.totalCaracteres;

	}
	//tiempo de ejecicion terminado
	tiempofinal = clock();

	//resultados
	cout << endl;
	cout << endl;
	cout << "Cantidad Total de lineas: " << contadorTotalLineas << endl;
	cout << "Cantidad Total de Palabras: " << contadorTotalPalabras << endl;
	cout << "Cantidad Total de Caracteres: " << contadorTotalCaracter << endl;
		cout << "Tiempo de ejecucion: " << calcular_tiempo(tiempoinicial, tiempofinal) << "segundos" << endl;

	return 0;
}
