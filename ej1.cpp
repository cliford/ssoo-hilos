#include <iostream>
#include <fstream>
#include <stdio.h>
#include <string.h>
#include <time.h>

using namespace std;

//Funcion para calcular tiempo de ejecucion
float calcular_tiempo(float tiempoinicial, float tiempofinal){
	//ecuacion para tiempo de ejecicion
	float ejecucion = (tiempofinal - tiempoinicial)/CLOCKS_PER_SEC;
	return ejecucion;
}

int contar_caracteres(ifstream &archivo){
	//variables
	int cont_caracteres = 0;
	char linea[3000];

	//ejecular solo en caso de archivo
	while(!archivo.eof()){
		archivo.getline(linea, 3000);

		//contador de variables se les suma el largo de la linea(carcateres)
		cont_caracteres = cont_caracteres + strlen(linea);
	}

	cout << "Cantidad de caracteres es: " << cont_caracteres << endl;
	return cont_caracteres;
}

int contar_palabras(ifstream &archivo){
	//variables
	int cont_palabras = 0;
	char linea[3000];

	//ejecular solo en caso de archivo
	while(!archivo.eof()){
		archivo.getline(linea, 3000);

		//obtener largo de linea
		int larg = strlen(linea);

		//recorrer linea
		for(int i=0;i<larg;i++){
			if((linea[i]==' ' || linea[i+1]== '\0')){ //condicion que limita los espacios como contar una palabra
			cont_palabras++;
			}
		}
	}
	

	cout << "Cantidad de palabras es: " << cont_palabras << endl;
	return cont_palabras;
}

int contar_lineas(ifstream &archivo){
	int cont_lineas = 0; //contador->lineas
	char linea[3000];

	//ejecular solo en caso de archivo
	while(!archivo.eof()){
		archivo.getline(linea, 3000);
		//contar lineas
		cont_lineas++;
	}

	cout << "Cantidad de lineas es: " << cont_lineas << endl;
	return cont_lineas;
}

int main(int argc, char* argv[]){
	//en caso de no cargar archivo no ejecutar
	if(argc <= 1){
		cout << "no ingreso archivos" << endl;
	}
	//variables
	ifstream archivo;
	int contTotalLineas = 0; 
	int contTotalPalabras = 0;
	int contTotalCaracteres = 0;
	float tiempoinicial, tiempofinal;
	
	//empezar a calcular el tiempo inicial
	tiempoinicial = clock();

	//ciclo para leer todos los archivos cargados mediante la ejecucion
	for(int i=1;i<argc;i++){
		cout << endl;
		cout << "Nombre del archivo: " << argv[i] << endl;
		
		//abrir archivo, contar lineas y cerrar
		archivo.open(argv[i]);
		contTotalLineas = contTotalLineas + contar_lineas(archivo);
		archivo.close();
		

		//abrir archivo, contar palabras y cerrar
		archivo.open(argv[i]);
		contTotalPalabras = contTotalPalabras + contar_palabras(archivo);
		archivo.close();

		//abrir archivo, contar caracteres y cerrar
		archivo.open(argv[i]);
		contTotalCaracteres = contTotalCaracteres + contar_caracteres(archivo);
		archivo.close();
	}
	//tiempo de ejecucion termiando
	tiempofinal = clock();

	//resutlados
		cout << "\n" << endl;
		cout << "Canntidad total de lineas es: " << contTotalLineas << endl;
		cout << "Canntidad total de palabras es: " << contTotalPalabras << endl;
		cout << "Canntidad total de caracteres es: " << contTotalCaracteres << endl;
		cout << "Tiempo de ejecucion: " << calcular_tiempo(tiempoinicial, tiempofinal) << "segundos" << endl;

	return 0;
}


