# Laboratorio 3 - Unidad 2 - Sistemas Operativos y Redes

Los programas desarrollados en las carpetas correspondientes

Existen 2 programa cuya funcion comparten, la cual es ingresar 1 o mas archivos de texto y estos programas procederan a 
estimar cuantas lineas, palabras, caracteres poseen los archivos ingresados, de ser mas de un archivo tambien se determinará
el total de lineas, palabras y caracteres de todos los archivos ingresados y por ultimo mostrará el tiempo que se demoró en ejecutar
todo el procedimiento medido en segundos.

## Estructura

El programa se ejecuta desde la terminal en donde se introducen los archivos que se querrán determinar las caracteristicas presentadas con aterioridad.



## Prerrequisitos

### Sistema Operativo

Linux y sus derivados.

### Make
Se puede compilar directamente desde la terminar con el siguiente comando:
archivo se refiere a los archivos utilizados en el codigo

"gcc archivo.cpp -o  archivo"

En caso de no tener instalado gcc.

`sudo apt install gcc`

-(opcional)Se requiere instalar un paquete llamado make, por terminar se puede ingresar el siguiente comando:

`sudo apt install make`



### Iniciar programa "compilar"

**Con make**

En la terminal situada en la carpeta con nuestros archivos se debe ejecutar el comando:

`make`

este compilará y generará todos los archivos requeridos solo para el ejercicio 1 (ej1.cpp)

**Sin make**

En la terminar situada en la carpeta con nuestros archivos se debe ejecutar el siguiente comando:

`g++ Programa.cpp Dato.cpp -o programa`

Para el ejecicio numero 2, se creó un ejecutable llamado "tutor", el cual al ejecutar compila automaticamente
el archivo "ej2.cpp" 
de no querer utilizar este ejutable se puede usar el comando

`g++ ej2.cpp -o ej2`

y este se compilará de manera manual

### Luego de compilar

Para ejecutar el programa una vez compilado se debe ejecutar el siguiente comando:

`./programa <archivo1> <archivo2> ...`


## Despliegue

La aplicación se debe ejecutar desde la terminal localizada en la carpeta que presenta los archivos de ésta. Se debe de ejecutar bajo el sistema operativo Linux (señalado en Prerrequisitos).

## Construido con
- Lenguaje c++: librería iostream.

## Autor
- Matias Vergara Arroyo 
- Contacto : mativ62@gmail.com

